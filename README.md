# PokerForJobApplication

This App aims to compare two hands in a poker-game.

It is written in Java. The main purpose is as a demonstration of my coding skills for a job-application.

You can see the source-code on [Gitlab](https://gitlab.com/michelle.rausch/pokerforjobapplication).

Compiled with Eclipse IDE 2021-09 and JavaSE-16. 

The documentation can be found [here](https://mrausch.mooo.com/projects/poker/doc/index.html).


An executable can be found [here](https://mrausch.mooo.com/projects/poker/executables/Poker.jar). This is the Test of the HandRanker. It satisfies the excercise goal. Will wrap it up in a proper main over the weekend. 
Set it as executable and run with `java -jar Poker.jar` (tested with openjdk 16.0.1).

You can run the different tests with e.g. `java  -cp Poker.jar tests/HandTest`. The follwing tests are available: CardTest, DeckTest, HandTest, HandLogicTest, HandRankerTest.
