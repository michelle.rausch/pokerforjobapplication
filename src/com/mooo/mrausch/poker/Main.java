/**
 * Poker
 * 
 * The aim of this app is to compare 2 hands in a poker-game. 
 * 2 hand have cards assigned to them and this app then outputs who has the better hand according to the rules of poker
 * 
 * This App is a coding sample for a Job-application.
 * 
 * @author Michelle Rausch
 */
package com.mooo.mrausch.poker;

import java.util.Scanner;

import com.mooo.mrausch.poker.logic.HandRanker;
import com.mooo.mrausch.poker.logic.PokerHandLogic;
import com.mooo.mrausch.poker.objects.Card;
import com.mooo.mrausch.poker.objects.Deck;
import com.mooo.mrausch.poker.objects.Hand;

/**
 * This is the Main-Class of the Poker-App
 * It contains the main-method, which will run when executing the .jar later.
 * @author Michelle Rausch
 *
 */
public class Main {

	/**
	 * The Main of the Poker app
	 * @param args Nothing yet
	 */
	public static void main(String[] args) {
		
		/*
		 * First print description of App
		 */
		System.out.println("Welcome to the Poker-App");
		System.out.println("This app aims to demonstrate my Coding skills with Java for a job-application.");
		System.out.println("You can run test by using the -cp option from the terminal e.g. > java -cp Poker.jar tests/HandTest <");
		System.out.println("Quit by inputing 'q' ");
		System.out.println("Discard a card (return to deck) and draw a new one by typing 'd', press enter and the a number, fold with 'f' ");
		System.out.println("I did not look up the actual rules of poker. This just demonstrates the comparison of two hands");
		System.out.println("\n\n------FIRST ROUND------\n");
		
		
		Deck deck = new Deck();
		HandRanker ranker = new HandRanker();
		
		/*
		 * Draw two hands
		 */
		

		deck.shuffle();
		
		Hand hand = new Hand();
		for (int i = 0; i < 5; i++) {
			hand.takeCardFromDeck(deck);	
		}
		
		Hand otherHand = new Hand();
		for (int i = 0; i < 5; i++) {
			otherHand.takeCardFromDeck(deck);		
		}
		
		
		
		
		
		
		
		char input = 'd';
		char otherInput = 'd';
		
		
		
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);
		
		while (input != 'q' && otherInput != 'q') { //quits, when one hand is quitting
			System.out.println("Hand one:");
			hand.printCards();
			
			System.out.println("What does hand one do?");
			
			input = scanner.next().charAt(0);
			
			switch (input) {
			case 'q': {
				System.out.println("Hand 1 quits");
				break;
			}
			case 'd' : {
				int inputWhichCard = 0 ;
				System.out.println("Which card will be discarded? (From 0 to 4)");
				inputWhichCard = scanner.nextInt();
				
				Card discarded;
				discarded = hand.discardCard(inputWhichCard);
				System.out.println("Hand 1 discarded " + discarded.getDescription());
				
				System.out.println("Hand 1 pulled " + hand.takeCardFromDeck(deck).getDescription());
				
				
				break;
			}
			case 'f' : {
				System.out.println("Hand 1 folds");
			}
			
			} //End switch for Input Hand 1
			
			System.out.println("Hand two:");
			otherHand.printCards();
			
			System.out.println("What does hand two do?");
			
			otherInput = scanner.next().charAt(0);
			switch (otherInput) {
			case 'q': {
				System.out.println("Hand 2 quits");
				break;
			}
			case 'd' : {
				int inputWhichCard = 0 ;
				System.out.println("Which card will be discarded? (From 0 to 4)");
				inputWhichCard = scanner.nextInt();
				
				Card discarded;
				discarded = hand.discardCard(inputWhichCard);
				System.out.println("Hand 2 discarded " + discarded.getDescription());
				
				System.out.println("Hand 2 pulled " + hand.takeCardFromDeck(deck).getDescription());
				
				
				break;
			}
			case 'f' : {
				System.out.println("Hand 2 folds");
				
				if (input == 'f') { //If hand 1 also folded, then compare
					System.out.println("\n-----COMPARE-----\n");
					
					//Output the description of the hands
					PokerHandLogic logic = new PokerHandLogic(hand);
					System.out.println("Hand 1 has a " +  logic.getPokerHandFull() );
					PokerHandLogic otherLogic = new PokerHandLogic(otherHand);
					System.out.println("Hand 2 has a " +  otherLogic.getPokerHandFull() );
					
					int comparison = ranker.compare(hand, otherHand);
					if (comparison == 0) {
						System.out.println("It was a draw\n");
					} else {
						if (comparison > 0) {
							System.out.println("Hand 1 wins!\n");
						} else {
							System.out.println("Hand 2 wins!\n");
						}
					}

					
				}
				
				break;
			}		
			
			}//End switch for Input Hand 2
				
			
			System.out.println("\n-----NEXT ROUND-----\n");
			
			
		} //End While
		
		

	}

}
