/**
 * Poker
 * 
 * The aim of this app is to compare 2 hands in a poker-game. 
 * 2 hand have cards assigned to them and this app then outputs who has the better hand according to the rules of poker
 * 
 * This App is a coding sample for a Job-application.
 * 
 * @author Michelle Rausch
 */
package com.mooo.mrausch.poker.logic;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import com.mooo.mrausch.poker.objects.Card;
import com.mooo.mrausch.poker.objects.Hand;
import com.mooo.mrausch.poker.objects.PokerHand;
import com.mooo.mrausch.poker.objects.Suit;
import com.mooo.mrausch.poker.objects.Value;

/**
 * This class analyzes the Hand for present Pokerhands
 * 
 * It has 
 * 		the cards which are to be analyzed
 * 		a description of the PokerHand (e.g. Straight Flush)
 * 		the rank within the PokerHand (e.g. 8) 
 * 
 * It can 
 * 		return the full description (e.g. Joker Straight Flush, )
 * 		Compare two Lists
 * 
 * @author Michelle Rausch
 *
 */
public class PokerHandLogic {
	
	/*
	 * Attributes
	 */
	/**
	 * The list of the cards to be analyzed
	 */
	private List<Card> cards = new ArrayList<Card>();
	
	/**
	 * The designation of the pokerhand 
	 */
	private PokerHand pokerHand;
	
	/**
	 * The value of the Pokerhand
	 */
	private Value value;
	
	/**
	 * These values breaks the draw, in case two hand have the same PokerHand
	 * If the first drawBreaker does not break the draw, findthe next highest value, etc
	 */
	private List<Value> drawBreaker = new ArrayList<Value>(5);
	
	
	/*
	 * Constructors
	 */
	/**
	 * Constructs the Logic with a list of cards, automatically analyzes at initialization
	 * @param cards The hand which will be handled by this logic
	 */
	public PokerHandLogic(List<Card> cards) {
		this.cards = cards;
		analyzeCards(); //so no null in getters
	}
	
	/**
	 * Constructs the Logic with a list of cards, automatically analyzes at initialization
	 * @param hand The hand which will be handled by this logic
	 */
	public PokerHandLogic(Hand hand) {
		this.cards = hand.getCards();
		analyzeCards(); //so no null in getters
	}
	
	
	
	/*
	 * Getter
	 */
	/**
	 * 
	 * @return The List of all cards analyzed by this logic
	 */
	public List<Card> getCards() {
		return cards;
	}
	
	/**
	 * The PokerHand
	 * @return The PokerHand
	 */
	public PokerHand getPokerHand() {
		return pokerHand;
	}
	
	/**
	 * 
	 * @return the highest value of the poker-hand
	 */
	public Value getValue() {
		return value;
	}
	
	/**
	 * 
	 * @return The Value which will break a draw
	 */
	public List<Value> getDrawBreaker() {
		return drawBreaker;
	}
	/**
	 * 
	 * @return The full name of the poker-hand
	 */
	public String getPokerHandFull() {
		return value.getName() + " " + pokerHand.getPokerHandName();
	}
	
	/*
	 * Setter
	 */
	/**
	 * Sets the List of cards and immediately runs an analysis
	 * @param cards the List of cards to be handled by this logic
	 */
	public void setCards(List<Card> cards) {
		this.cards = cards;
		this.analyzeCards();
	}
	
	/*
	 * Methods
	 */
	/**
	 * Analyzes the cards and updates the PokerHanddescription
	 */
	public void analyzeCards() {
		pokerHand = null;
		value = null;
		
		sortCards(); // start by sorting
		
		//Before any analysis check, if hand is valid = 5 cards		
		if (cards.size() == 5) {
			
			if (!scanForStraightFlush()) {
				if (!scanForFourOfAKind()) {
					if (!scanForFullHouse()) {
						if (!scanForStraight()) {
							if (!scanForFlush()) {
								if (!scanForThreeOfAKind()) {
									if (!scanForTwoPair()) {
										if (!scanForPair()) {
											scanForHighCard();
										}
									}
								}								
							}
						}
					}
				}
			}

		} else  {
			System.out.println("You don't have 5 hands in your hand, please discard or draw from deck!");
		}		
	}
	
	/**
	 * scans the cards for a Straight flush
	 * (as by instructions:)
	 * Straight flush: 5 cards of the same suit with consecutive values. Ranked by the highest card in the hand.
	 * @return If cards contain a StraightFlush
	 */
	private boolean scanForStraightFlush() { //private, so it cannot be used from outside this class. Sanity Checks are performed there!
		
		/*
		 * start with first card, check, if all following cards have same suit 
		 * since there's only 5 cards in a hand, this is sufficient, check has been performed in analyzeCards(), TODO: add here too
		 * then check for consecutive values, is possible, because we sorted cards by value beforehand
		 */
		
		Suit suitReference = cards.get(0).getSuit();
		for (int i = 1; i < 5; i++) {
			if (!cards.get(i).getSuit().equals(suitReference)) {
				return false; //if a suit is different, it CANNOT be a straight flush
			}
		}	
		
		//compares i to i+1, if value_(i+1) not (value_i +1), then no straight flush
		for (int i = 0; i < 3; i++) { 
			if (cards.get(i).getValue().getRank() + 1 != cards.get(i+1).getValue().getRank()) {
				return false;
			}
		}
		
		pokerHand = PokerHand.STRAIGHTFLUSH;
		value = cards.get(4).getValue();

		return true;
	}
	
	/**
	 * scans the cards for a Four of a Kind
	 * (as by instructions:)
	 * 4 cards with the same value. Ranked by the value of the 4 cards.
	 * Also sets the Drawbreaker
	 * @return If the cards contain a Four of a Kind
	 */
	private boolean scanForFourOfAKind() {
		
		/*
		 * Scan for each Value, if a value is contained 4 times, return value and set FoaK as true
		 */
		drawBreaker.clear(); //Clear draw-breaker to get any from before out, alternative run at end
		boolean hasFourOfAKind = false; //Need to run over all values to keep track of the drawBreaker
		
		
		for (int i = 0; i < Value.values().length; i++) { //iterate in order, so drawBreaker works
			
			int occurence = 0;

			
			for (Card card : cards) {
				if (card.getValue().equals(Value.values()[i])) {
					occurence = occurence +1;
				}
			}
			
			if (occurence == 4 ) {
				value = Value.values()[i];
				pokerHand = PokerHand.FOUROFAKIND;
				hasFourOfAKind = true;
			} else {
				drawBreaker.add(Value.values()[i]); //If This is not the FoaK, it's possible candidate for drawBreaker
			}
		}

		return hasFourOfAKind;
	}
	
	/**
	 * scans the cards for a Full House
	 * (as by instructions:)
	 * 3 cards of the same value, with the remaining 2 cards forming a pair. Ranked by the value of the 3 cards. 
	 * The Value of the pair breaks the draw
	 * @return If the cards contain a Full House
	 */
	private boolean scanForFullHouse() {
	
		/*
		 * Scan for each Value. 
		 * Track occurance. 
		 * If one occurance is 2, save as boolean.
		 * If one occurance is also 3, save as boolean, and valueCandidate
		 * After iterating over values: both booleans are true, then FullHouse!
		 */
		drawBreaker.clear(); //Clear drawBreaker at beginning
		boolean hasTriplet = false;
		Value tripletValue = null;
		boolean hasPair = false;
		
		for (int i = 0; i < Value.values().length; i++) {
			
			int occurence = 0;
			
			for (Card card : cards) {
				if (card.getValue().equals(Value.values()[i])) {
					occurence = occurence +1;
				}
			}
			
			if (occurence == 2) {
				drawBreaker.add(Value.values()[i] ); //Sets the pair as a drawBreaker
				hasPair = true ;
			} else {
				if (occurence == 3) {
					hasTriplet = true;
					tripletValue = Value.values()[i];
				}
			}	
		}

		
		if (hasPair && hasTriplet) {
			value = tripletValue;
			pokerHand = PokerHand.FULLHOUSE;				
			return true;
		} else {
			return false;
		}
	}
	
	
	/**
	 * scans the cards for a Straight
	 * (as by instructions:)
	 * Hand contains 5 cards with consecutive values. Hands which both contain a straight are ranked by their highest card.
	 * @return If the cards contain a Straight
	 */
	private boolean scanForStraight() {
		
		//sort by value
		cards.sort(new Comparator<Card>() {

			@Override
			public int compare(Card card, Card otherCard) { 
				return card.getValue().getRank() - otherCard.getValue().getRank() ;
			}
		});
		
		//iterate ofer all cards, check, if one is out of order 
		//compares rank of 0th card with first, etc, when one out of order, return false		
		for (int i = 0; i < 3; i++) { 
			if (cards.get(i).getValue().getRank() + 1 != cards.get(i+1).getValue().getRank()) {
				sortCards(); //sorts back to original sortingf
				return false; //do return here instead of using a boolean, because loop does not need to be completed, also save an if later
			}
		}
		
		//This is only executed, when no out of order
		pokerHand = PokerHand.STRAIGHT;
		value = cards.get(4).getValue();
		sortCards(); //sorts back to original sorting
		return true;
	}
	
	/**
	 * scans the cards for a Flush
	 * (as by instructions:)
	 * Hand contains 5 cards of the same suit. Hands which are both flushes are ranked using the rules for High Card.
	 * @return If the cards contain a Flush
	 */
	private boolean scanForFlush() {
		
		/*
		 * All suits must be the same, so start with fist and iterate, if any is different, return false
		 */
		
		//Also keep track of HighCard
		Value highestValue = cards.get(0).getValue();
		
		
		
		for (int i = 0; i < 4; i++) { //Updates the HighCard
			if (highestValue.getRank() < cards.get(i+1).getValue().getRank()) {
				highestValue = cards.get(i+1).getValue();
			}
			
			if (!cards.get(i).getSuit().equals(cards.get(i+1).getSuit()) ) {
				return false;
			}
		}
		
		pokerHand = PokerHand.FLUSH;
		value = highestValue;
		return true;
	}
	
	/**
	 * scans the cards for a Three of a Kind
	 * (as by instructions:)
	 * Three of a Kind: Three of the cards in the hand have the same value. Hands which both contain three of a kind are ranked by the value of the 3 cards.
	 * The other two cards break the draw
	 * @return If the card contains a Three of a Kind
	 */
	private boolean scanForThreeOfAKind() {
		
		/*
		 * Analogue to scanForFourOfAKind()
		 * 
		 * Keep track of occurence.

		 */
		
		boolean hasThreeOfAKind = false; //Needs to use a boolean here, because we need to run over all values to get all drawBreakers
		
		
		drawBreaker.clear(); //clear Drawbreaker in the beginning
		for (int i = 0; i < Value.values().length; i++) {
			int occurence = 0;
			
			for (Card card : cards) {
				if (card.getValue().equals(Value.values()[i])) {
					occurence = occurence +1;
				}
			}
			
			if (occurence == 3 ) {
				value = Value.values()[i];
				pokerHand = PokerHand.THREEOFAKIND;				
				hasThreeOfAKind = true;
			} else {
				drawBreaker.add(Value.values()[i]);
			}
		}
		sortDrawBreaker(); //So it comes pre-sorted for HandRanker
		return hasThreeOfAKind;
	}
	
	/**
	 * scans the cards for a TwoPair
	 * (as by instructions:)
	 * Two Pairs: The hand contains 2 different pairs. Hands which both contain 2 pairs are ranked by the value of their highest pair. 
	 * Hands with the same highest pair are ranked by the value of their other pair. 
	 * If these values are the same the hands are ranked by the value of the remaining card.
	 * @return If the cards contain a TwoPair
	 */
	private boolean scanForTwoPair() {
		
		/*
		 * Similar to FullHouse
		 */

		drawBreaker.clear();
		drawBreaker.add(null);
		drawBreaker.add(null);//This is weird. I should have stayed with an array for drawBreaker... TODO maybe
		
		Value highestPairValue = null; //The highest valued pair
		int numberOfPairs = 0; //For checking, if there are 2 pairs
		
		for (int i = 0; i < Value.values().length; i++) {
		
			int occurence = 0; //how often this Value has been found among the decks
			
			for (Card card : cards) {
				if (card.getValue().equals(Value.values()[i])) {
					occurence = occurence +1;
				}
			}
			
			/*
			 * The second drawBreaker should be the one Card, which only occures once
			 */
			if (occurence ==1) {
				drawBreaker.set(1,Value.values()[i]);//Set as the SECOND drawbreaker, because we check for the other pair first.
			}
			
			if (occurence == 2) {
				numberOfPairs = numberOfPairs + 1;
				if (highestPairValue == null) { //If true, then first pair. Also candidate for first drawBreaker
					highestPairValue = Value.values()[i];
					drawBreaker.set(0, Value.values()[i]);
				} else { //second Pair
					if ( highestPairValue.getRank() < Value.values()[i].getRank()) {
						highestPairValue = Value.values()[i];
					}	else {
						drawBreaker.set(0, Value.values()[i]); //If second pair smaller, then second pair is drawBreaker
					}
				}
	
			} 	
		}
		
		/*
		 * Do NOT run DrawBreaker here!
		 */
		
		if (numberOfPairs == 2) {
			value = highestPairValue;
			pokerHand = PokerHand.TWOPAIR;	
			return true;
		} else {
			return false;
		}
		 
	}
	
	/**
	 * scans the cards for a Pair
	 * (as by instructions:)
	 * Pair: 2 of the 5 cards in the hand have the same value. 
	 * Hands which both contain a pair are ranked by the value of the cards forming the pair. 
	 * If these values are the same, the hands are ranked by the values of the cards not forming the pair, in decreasing order.
	 * @return If the cards contain a Pair

	 */
	private boolean scanForPair() {
		
		/*
		 * Iterate over all values, check, if pair exists. 
		 */
		
		drawBreaker.clear(); //First clear the drawBreaker
		boolean hasPair = false;
		
		
		for (int i = 0; i < Value.values().length; i++) {
			
			
			int occurence = 0; //how often this Value has been found among the decks
			
			for (Card card : cards) {
				if (card.getValue().equals(Value.values()[i])) {
					occurence = occurence +1;
				}
			}
			
			if (occurence == 2) {
				value = Value.values()[i];
				pokerHand = PokerHand.PAIR;
				hasPair =  true;	
			} else {
				drawBreaker.add(Value.values()[i]); //If card is not in pair, add to drawbreaker
			}
		}
		
		sortDrawBreaker(); //So it comes pre-sorted for HandRanker
		
		return hasPair;
	}
	
	/**
	 * scans the cards for HighCard
	 * (as by instructions:)
	 * Hands which do not fit any higher category are ranked by the value of their highest card. 
	 * If the highest cards have the same value, the hands are ranked by the next highest, and so on.
	 * @return If the cards contain a High-Card (trivially true)
	 */
	private boolean scanForHighCard() {
		Value highestValue = cards.get(0).getValue();
		drawBreaker.clear(); //First clear the drawBreaker and 
		cards.forEach(card -> {
			drawBreaker.add(card.getValue());
		});
		
		for (int i=1 ; i<4 ; i++) {
			if (cards.get(i).getValue().getRank() > highestValue.getRank()) {
				highestValue = cards.get(i).getValue();
			}
		}
		
		drawBreaker.remove(highestValue); //remove the highest value form the drawBreaker, leaving only the other 4 to compare in HandRanker
		sortDrawBreaker(); //So it comes pre-sorted for HandRanker
		value = highestValue;
		pokerHand = PokerHand.HIGHCARD;
		return true;
	}
	
	/**
	 * 	sorts The drawBreaker. Do NOT use, if 
	 */
	private void sortDrawBreaker() {
		drawBreaker.sort(new Comparator<Value>() {
			@Override
			public int compare(Value value, Value otherValue) {
				return value.getRank() - otherValue.getRank();
			}
			
		});
	}
	
	/**
	 * sorts the card first by Suit, then by Value
	 */
	public void sortCards() {
		cards.sort(new Comparator<Card>() {

			@Override
			public int compare(Card card, Card otherCard) {	
				//TODO more efficient sorting
				return (card.getSuit().ordinal()- otherCard.getSuit().ordinal()) * Value.values().length + (card.getValue().ordinal() - otherCard.getValue().ordinal() );
			}
		});
		
	}
	
	
	
}
