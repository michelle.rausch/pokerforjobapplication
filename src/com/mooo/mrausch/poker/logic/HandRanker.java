/**
 * Poker
 * 
 * The aim of this app is to compare 2 hands in a poker-game. 
 * 2 hand have cards assigned to them and this app then outputs who has the better hand according to the rules of poker
 * 
 * This App is a coding sample for a Job-application.
 * 
 * @author Michelle Rausch
 */
package com.mooo.mrausch.poker.logic;

import java.util.Comparator;
import java.util.List;


import com.mooo.mrausch.poker.objects.Hand;
import com.mooo.mrausch.poker.objects.Value;

/**
 * This Comparator compares hands
 * Allows multiple hands to be ranked at once
 * 
 * @author Michelle Rausch
 *
 */
public class HandRanker implements Comparator<Hand> {

	/**
	 * If rank of Hand > rank of otherHand, then positive return
	 * @return a negative integer, zero, or a positive integer as the first argument is less than, equal to, or greater than the second.
	 */
	@Override
	public int compare(Hand hand, Hand otherHand) {
		PokerHandLogic logicForHand = new PokerHandLogic(hand);
		PokerHandLogic logicForOtherHand = new PokerHandLogic(otherHand);
		
		//First Rank by PokerHand.rank, then by value
		int differenceHandRank = logicForHand.getPokerHand().getRank() - logicForOtherHand.getPokerHand().getRank(); 
		
		if (differenceHandRank == 0) {
			int differenceValueRank = logicForHand.getValue().getRank() - logicForOtherHand.getValue().getRank();
			if (differenceValueRank == 0) {
				
				/*
				 * Go over the List of drawBreakers, how many ever there are
				 */
				List<Value> drawBreaker = logicForHand.getDrawBreaker();
				List<Value> otherDrawBreaker = logicForOtherHand.getDrawBreaker();
				for (int i = 0; i < drawBreaker.size(); i++) {
					int differenceDrawBreakerRank = drawBreaker.get(i).getRank() - otherDrawBreaker.get(i).getRank();
					if (differenceDrawBreakerRank == 0) {
						//Intentionally left blank for better readability, If nothing happens here over all drawBreakers, it will return 0 at end
					} else {
						return differenceDrawBreakerRank;
					}

				}				
			} else {
				return differenceValueRank; //If value of same PokerHand is not draw
			}
			
		} else {
			return differenceHandRank;// If hand is not draw
		}
		
		return 0;

	}
		


}
