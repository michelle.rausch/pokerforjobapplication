/**
 * Poker
 * 
 * The aim of this app is to compare 2 hands in a poker-game. 
 * 2 hand have cards assigned to them and this app then outputs who has the better hand according to the rules of poker
 * 
 * This App is a coding sample for a Job-application.
 * 
 * @author Michelle Rausch
 */
package com.mooo.mrausch.poker.exceptions;

/**
 * This exception is thrown, if the deck is empty 
 * @author Michelle Rausch
 *
 */
public class EmptyDeckException extends Exception {
	
	
	
	/**
	 * Constructor with standard Text
	 */
	public EmptyDeckException() {
		super("The deck is empty!");
		
	}

	/**
	 * Constructor with custom Text
	 * @param message The message belonging to this exception
	 */
	public EmptyDeckException(String message) {
		super(message);
	}

	/**
	 * SerialVersionUID not required for this Project
	 */
	private static final long serialVersionUID = 1L;
	
	

}
