/**
 * Poker
 * 
 * The aim of this app is to compare 2 hands in a poker-game. 
 * 2 hand have cards assigned to them and this app then outputs who has the better hand according to the rules of poker
 * 
 * This App is a coding sample for a Job-application.
 * 
 * @author Michelle Rausch
 */
package com.mooo.mrausch.poker.objects;

/**
 * A card in a poker game
 * A Card has a Suit and a Value
 * A Card can show its suit and value as well as a description of itself
 * @author Michelle Rausch
 *
 */
public class Card {
	
	/*
	 * Attributes
	 */
	/**
	 * The suit of the card
	 */
	private Suit suit;
	
	/**
	 * The value of the card
	 */
	private Value value;
	
	/*
	 * Constructors
	 */
	/**
	 * constructs the card with a suit and a value 
	 * @param suit The suit of the card
	 * @param value The value of the card
	 */
	public Card(Suit suit, Value value) {
		this.suit = suit;
		this.value = value;
	}

	/*
	 * Getters & Setters
	 * setters don't really make sense, because you can't change the value of a card
	 */
	
	/**
	 * 
	 * @return The suit of the card
	 */
	public Suit getSuit() {
		return suit;
	}

	/**
	 * 
	 * @return The value of the card
	 */
	public Value getValue() {
		return value;
	}
	
	/**
	 * 
	 * @return A description of the card
	 */
	public String getDescription() {
		
		return value.getName() + " of " + suit.getName();
	}
	

	@Override
	public String toString() {
		return "Card [suit=" + suit + ", value=" + value + "]";
	}
	
	
	
	
	
}
