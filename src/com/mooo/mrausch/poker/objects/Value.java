/**
 * Poker
 * 
 * The aim of this app is to compare 2 hands in a poker-game. 
 * 2 hand have cards assigned to them and this app then outputs who has the better hand according to the rules of poker
 * 
 * This App is a coding sample for a Job-application.
 * 
 * @author Michelle Rausch
 */
/**
 * 
 */
package com.mooo.mrausch.poker.objects;

/**
 * This enum contains the Value of a poker-card
 * 
 * Each card also has a value which is one of 2, 3, 4, 5, 6, 7, 8, 9, 10, jack, queen, king, ace (denoted 2, 3, 4, 5, 6, 7, 8, 9, T, J, Q, K, A)
 * 
 * 
 * @author Michelle Rausch
 *
 */
public enum Value {
	/** 2 */
	TWO('2', "two", 2), 
	/** 3 **/
	THREE('3', "three", 3),
	/** 4 */
	FOUR('4', "four", 4), 
	/** 5 */
	FIVE('5', "five", 5),
	/** 6 */
	SIX('6', "six", 6), 
	/** 7 */
	SEVEN('7', "seven", 7),
	/** 8 */
	EIGHT('8', "eight", 8), 
	/** 9 */
	NINE('9', "nine", 9), 
	/** 10, The ten is denoted as T as by instructions */
	TEN('T', "ten", 10),
	/** Jack */
	JACK('J', "jack", 11),
	/** Queen */
	QUEEN('Q', "queen", 12),
	/** King */
	KING('K', "king", 13),
	/** Ace */
	ACE('A', "ace", 14);
	
	/*
	 * Attributes
	 */
	/**
	 * The singe-letter denotion of the value
	 */
	private char denotion;
	
	/**
	 * The written name of the value
	 */
	private String name;
	
	/**
	 * The rank of the Value as translated to integers, from low to high
	 */
	private int rank;
	
	/*
	 * Contructors
	 */
	/**
	 * Constructs the Value with a denotion, the written name and its rank
	 * @param denotion The single-letter denotion of the value
	 * @param name The written Name of the Value
	 * @param rank The rank of the Value from low to high
	 */
	private Value(char denotion, String name, int rank) {
		this.denotion = denotion;
		this.name = name;
		this.rank = rank;
	}
	
	/*
	 * Getters
	 */
	/**
	 * 
	 * @return The singe-letter denotion of the value
	 */
	public char getDenotion() {
		return denotion;
	}
	
	/**
	 * 
	 * @return The written name of the value
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * 
	 * @return The rank of the Value
	 */
	public int getRank() {
		return rank;
	}

}
