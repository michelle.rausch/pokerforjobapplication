/**
 * Poker
 * 
 * The aim of this app is to compare 2 hands in a poker-game. 
 * 2 hand have cards assigned to them and this app then outputs who has the better hand according to the rules of poker
 * 
 * This App is a coding sample for a Job-application.
 * 
 * @author Michelle Rausch
 */
package com.mooo.mrausch.poker.objects;

import java.util.ArrayList;
import java.util.List;

import com.mooo.mrausch.poker.exceptions.EmptyDeckException;

/**
 * This class is the hand of a poker-player
 * The hand has 
 * 		5 cards
 * 
 * The hand can
 * 		return a list of all cards
 * 		the description of the poker-hand
 * 		discard a card
 * 		discard all cards
 * 
 * 
 * 
 * @author Michelle Rausch
 * 
 *
 */
public class Hand {
	
	/*
	 * Attributes
	 */
	
	/**
	 * The list of the cards in the hand
	 */
	private List<Card> cards = new ArrayList<Card>();
	
	/*
	 * Constructors
	 */
	/**
	 * default constructor
	 */
	public Hand() {
		
	}
	
	/*
	 * Getter 
	 */
	/**
	 * 
	 * @return The list of the cards in the hand
	 */
	public List<Card> getCards() {
		return cards;
	}

	/*
	 * Methods
	 */
	/**
	 * Pulls a card from the top of the deck
	 * @param deck The deck the card will be pulled from
	 * @return the Card which was just pulled
	 */
	public Card takeCardFromDeck(Deck deck) {
		Card pulledCard = null;
		try {
			pulledCard = deck.pullCardfromTop();

		} catch (EmptyDeckException e) {
			System.err.println("The Deck was empty!");
		}
		
		if (pulledCard != null) {
			cards.add(pulledCard);
		}

		return pulledCard;
	}
	
	/**
	 * discards all cards
	 * @return The cards, which were discarded
	 */
	public List<Card> discardAllCards() {
		List<Card> cardsToBeDiscarded = new ArrayList<Card>(cards); //constructs the temporary list from the cards whcih are on hand right now
		cards.clear();
		return cardsToBeDiscarded;
	}
	
	/**
	 * discards the card at index
	 * @param index From which position the card will be discarded from the hand.
	 * @return The card which has been discarded
	 */
	public Card discardCard(int index) {
		
		Card cardToDiscard;
		if (index >=0 && index < cards.size()) {
			cardToDiscard = cards.get(index);
			cards.remove(index);
		} else {
			cardToDiscard = null; //TODO: errorhandling, if index not in range
		}
		return cardToDiscard;
	}
	
	/**
	 * Prints all cards on the hand
	 */
	public void printCards() {
		cards.forEach(card -> {
			System.out.println(card.getDescription());
		});
	}

}
