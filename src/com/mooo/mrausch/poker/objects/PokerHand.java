/**
 * Poker
 * 
 * The aim of this app is to compare 2 hands in a poker-game. 
 * 2 hand have cards assigned to them and this app then outputs who has the better hand according to the rules of poker
 * 
 * This App is a coding sample for a Job-application.
 * 
 * @author Michelle Rausch
 */
package com.mooo.mrausch.poker.objects;


/**
 * This is the Pokerhand
 * 
 * It has a 
 * 		name of the PokerHand (e.g. Straight Flush)
 * 
 * It can 
 * 		run a test on a list of Cards, if this is present.
 * 
 * @author Michelle Rausch
 *
 */
public enum PokerHand {
	/**
	 * High Card
	 */
	HIGHCARD("High Card", 0), 
	/**
	 * Pair
	 */
	PAIR("Pair", 1), 
	/**
	 * Two Pair
	 */
	TWOPAIR("Two Pairs", 2), 
	/**
	 * Three of a Kind
	 */
	THREEOFAKIND("Three of a Kind", 3), 
	/**
	 * Straight
	 */
	STRAIGHT("Straight", 4), 
	/**
	 * Flush
	 */
	FLUSH("Flush", 5), 
	/**
	 * Full House
	 */
	FULLHOUSE("Full House", 6), 
	/**
	 * Four of a Kind
	 */
	FOUROFAKIND("Four of a Kind", 7),
	/**
	 * Straight Flush
	 */
	STRAIGHTFLUSH("Straight Flush", 8); 
	
	
	/*
	 * Attributes
	 */
	/**
	 * The written Name of the Poker-Hand
	 */
	private String pokerHandName;
	/**
	 * The rank of the Poker Hand, from low to high 
	 */
	private int rank;
	
	/*
	 * Constructors
	 */
	/**
	 * Constructs the PokerHand
	 * @param pokerHandName the writen name of the PokerHand
	 * @param rank The rank of the PokerHand from low to high
	 */
	private PokerHand(String pokerHandName, int rank) {
		this.pokerHandName = pokerHandName;
		this.rank = rank;
	}

	/*
	 * Getter
	 */
	/**
	 * 
	 * @return The written Name of the Poker-Hand
	 */
	public String getPokerHandName() {
		return pokerHandName;
	}
	
	/**
	 * 
	 * @return The Rank of the Poker-Hand from low to high value
	 */
	public int getRank() {
		return rank;
	}
	
	

}
