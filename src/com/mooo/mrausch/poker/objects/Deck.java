/**
 * Poker
 * 
 * The aim of this app is to compare 2 hands in a poker-game. 
 * 2 hand have cards assigned to them and this app then outputs who has the better hand according to the rules of poker
 * 
 * This App is a coding sample for a Job-application.
 * 
 * @author Michelle Rausch
 */
package com.mooo.mrausch.poker.objects;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.mooo.mrausch.poker.exceptions.EmptyDeckException;

/**
 * This class contains a deck of poker cards
 * A deck starts with 52 Card.
 * 
 * A Deck has a 
 * 		list of cards (ordered)
 * 
 * A Deck can be 
 * 		resetted, which would be the gathering of cards
 * 		shuffeled
 * 		
 * A Deck can have
 * 		a Card removed
 * 			from top / any position
 * 		a Card added
 * 			to top / any position
 * 
 * A Deck can output a list of all cards
 *  
 * @author Michelle Rausch
 *
 */
public class Deck {
	
	/*
	 * Atributes
	 */
	/**
	 * The list of all Card, which this deck contains
	 */
	private List<Card> cards = new ArrayList<Card>();
	
	 
	/*
	 * Constructors
	 */
	/**
	 * construct a standard deck with 52 Card
	 */
	public Deck() {
		reset();
	}
	
	/*
	 * Getter & Setter
	 */

	/**
	 * 
	 * @return The list of all Card
	 */
	public List<Card> getCards() {
		return cards;
	}


	/**
	 * Sets The list of all Card
	 * @param cards The list of all Card to be set
	 */
	public void setCards(List<Card> cards) {
		this.cards = cards;
	}
	
	/*
	 * Methods
	 */

	/**
	 * Resets all cards, brings the Deck back into an ordered form
	 */
	public void reset() {
		cards.clear(); //first reset the list of cards
		for (Suit suit : Suit.values()) { //iterate over every Suit
			
			for (Value value : Value.values()) { //iterate of every Value
				
				cards.add(new Card(suit, value));
			}
			
		}
	}
	
	/**
	 * Adds a Card on top of the Deck
	 * @param cardToAdd the Card to add onm top of the Deck
	 */
	public void addCardOnTop(Card cardToAdd) {
		cards.add(cardToAdd);
	}
	
	/**
	 * Adds a card into the Deck
	 * If the position is not present, it will add on top of the Deck
	 * @param position where the Card is added
	 * @param cardToAdd the card which will be added
	 */
	public void addCard(int position, Card cardToAdd) {
		if (position < 0  || position > cards.size() )  { //checks, if position is too large or nonsensical
			cards.add(cardToAdd);
		} else {
			cards.subList(0, position).add(cardToAdd);
		}
	}
	
	/**
	 * Shuffles the cards in the Deck
	 */
	public void shuffle() {
		Collections.shuffle(cards);
	}
	

	/**
	 * 
	 * @param position The index, from which the card will be pulled
	 * @return the pulled card, null if card does not exist
	 * @throws IndexOutOfBoundsException If a card is pulled from an index not inside the deck
	 */
	public Card pullCard(int position) throws IndexOutOfBoundsException{
		Card pulledCard;
		
		
		if (position < 0  || position > cards.size() )  { //checks, if position is too large or nonsensical	
			pulledCard = null;	
			throw new IndexOutOfBoundsException("This card is not inside the deck!"); 
			
		} else {
			pulledCard = cards.get(position);
			cards.remove(position);
		} 
		
		return pulledCard;
	}
	
	/**
	 * Pulls a card from the top of the deck
	 * @return The Card on top of the deck, null, if deck is empty
	 * @throws EmptyDeckException If you try to pull a card from an empty deck
	 */
	public Card pullCardfromTop() throws EmptyDeckException {
		Card pulledCard;
		if (cards.isEmpty()) {
			pulledCard = null;
			throw new EmptyDeckException();
		} else {
			pulledCard = cards.get(cards.size()-1);
			cards.remove(cards.size()-1);
		}

		return pulledCard;
	}
	
	/**
	 * prints the deck to sysout
	 */
	public void printDeck() {
		System.out.println("print the Deck");
		cards.forEach(card -> {
			System.out.println(card.getDescription());
		});
	}
	

}
