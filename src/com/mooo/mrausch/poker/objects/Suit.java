/**
 * Poker
 * 
 * The aim of this app is to compare 2 hands in a poker-game. 
 * 2 hand have cards assigned to them and this app then outputs who has the better hand according to the rules of poker
 * 
 * This App is a coding sample for a Job-application.
 * 
 * @author Michelle Rausch
 */
package com.mooo.mrausch.poker.objects;

/**
 * This Enum describes the suits for a poker-card
 * The following suits exist:
 * 		Heart, Spade, Club, Diamond
 * 
 * They each have an associated name (e.g. heart), which is the enum-name but lower-case 
 * 
 * 
 * @author Michelle Rausch
 *
 */
public enum Suit {
	/**
	 * Spades
	 */
	SPADE("spades"),
	/**
	 * Heart
	 */
	HEART("heart"),
	/**
	 * Club
	 */
	CLUB("club"),
	/**
	 * Diamond
	 */
	DIAMOND("diamond");
	
	/*
	 * Attributes
	 */
	/**
	 * The human-readable name of the suit
	 */
	private String name;
	
	/*
	 * Contructor
	 */
	/**
	 * constructs the suit with a human-readable name
	 * @param name The human-readable name of the suit
	 */
	private Suit(String name) {
		this.name = name;
	}
	
	/*
	 * Getters
	 */
	/**
	 * 
	 * @return The human-readable name of the suit
	 */
	public String getName() {
		return name;
	}
}
