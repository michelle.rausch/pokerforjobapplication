/**
 * Poker
 * 
 * The aim of this app is to compare 2 hands in a poker-game. 
 * 2 hand have cards assigned to them and this app then outputs who has the better hand according to the rules of poker
 * 
 * This App is a coding sample for a Job-application.
 * 
 * @author Michelle Rausch
 */
package tests;

import com.mooo.mrausch.poker.exceptions.EmptyDeckException;
import com.mooo.mrausch.poker.objects.Card;
import com.mooo.mrausch.poker.objects.Deck;
import com.mooo.mrausch.poker.objects.Suit;
import com.mooo.mrausch.poker.objects.Value;

/**
 * This tests the deck of cards
 * @author Michelle Rausch
 *
 */
public class DeckTest {

	/**
	 * Runs the test
	 * @param args Not functional
	 */
	public static void main(String[] args) {
		
		/*
		 * The Deck contains all possible cards
		 * for each suit {2...A}
		 * A total of 4*13 = 52 cards
		 * 
		 * each card is unique
		 * 
		 * therefore: 
		 * 		A Deck is a list of Cards
		 * 		The deck can be shuffled
		 * 		a Card can be taken: deck.pull() returns card, removes card from deck
		 * 
		 * tests: 
		 * 	gather cards
		 * 		output
		 * 	shuffle cards
		 * 		output
		 * 	pull cards
		 * 		test if sucessfully removed
		 * 		test, if correct card removed
		 * 	pull all cards -> empty deck
		 * 	
		 */
		
		System.out.println("initialize Deck");
		Deck deck = new Deck();
		
		System.out.println("print the Deck");
		deck.printDeck();
		

		System.out.println("\n-------------NEXT TEST-------------\n" );
		/*
		 * Shuffle the deck
		 */
		System.out.println("Shuffle the deck");
		deck.shuffle();

		System.out.println("print the Deck");
		deck.printDeck();

		System.out.println("\n-------------NEXT TEST-------------\n" );
		
		/*
		 * reset the deck
		 */
		System.out.println("reset the Deck");
		deck.reset();
		System.out.println("print the Deck");
		deck.printDeck();
		
		System.out.println("\n-------------NEXT TEST-------------\n" );
		
		/*
		 * add cards
		 */
		
		System.out.println("add card on top");
		deck.addCardOnTop(new Card(Suit.CLUB, Value.FIVE));
		
		System.out.println("print the Deck");
		deck.printDeck();
		
		System.out.println("\n-------------NEXT TEST-------------\n" );
		System.out.println("add card at impossible position");
		
		deck.addCard(-4, new Card(Suit.SPADE, Value.ACE));
		
		deck.addCard(90, new Card(Suit.SPADE, Value.ACE));
		
		System.out.println("add card at Position 1");
		deck.addCard(1, new Card(Suit.SPADE, Value.ACE));
		
		System.out.println("print the Deck");
		deck.printDeck();
		
		System.out.println("\n-------------NEXT TEST-------------\n" );
		
		/*
		 * Pull cards
		 */
		deck.reset();
		
		System.out.println("card on top: " + deck.getCards().get(deck.getCards().size()-1));
		
		try {
			System.out.println("pull card from top: " + deck.pullCardfromTop().getDescription());
		} catch (EmptyDeckException e) {
			System.err.println("The deck was empty");
		}
		
		System.out.println("card on top: " + deck.getCards().get(deck.getCards().size()-1));
		
		System.out.println("\n-------------NEXT TEST-------------\n" );
		
		System.out.println("card on bottom: " + deck.getCards().get(0));
		
		System.out.println("pull card from top: " + deck.pullCard(0));
		
		System.out.println("card on top: " + deck.getCards().get(0));
		
		System.out.println("\n-------------TEST EXCEPTIONS-------------\n" );
		
		System.out.println("pull card from invalid range");
		try {
			System.out.println("card -1: " + deck.pullCard(-1).getDescription());
		} catch (IndexOutOfBoundsException e) {
			e.printStackTrace();
		}	
		
		try {
			System.out.println("card 53: " +  deck.pullCard(53).getDescription());
		} catch (IndexOutOfBoundsException e) {
			e.printStackTrace();
		}
		
		System.out.println("Empty deck");
		deck.getCards().clear();
		System.out.println("... and pull card");
		try {
			deck.pullCardfromTop();
		} catch (EmptyDeckException e) {
			System.out.println("empty deck handled");
		}
		
		
	}
	
	
	


}
