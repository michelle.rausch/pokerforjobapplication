/**
 * Poker
 * 
 * The aim of this app is to compare 2 hands in a poker-game. 
 * 2 hand have cards assigned to them and this app then outputs who has the better hand according to the rules of poker
 * 
 * This App is a coding sample for a Job-application.
 * 
 * @author Michelle Rausch
 */
package tests;

import java.util.ArrayList;
import java.util.List;

import com.mooo.mrausch.poker.logic.PokerHandLogic;
import com.mooo.mrausch.poker.objects.Card;
import com.mooo.mrausch.poker.objects.Deck;
import com.mooo.mrausch.poker.objects.Suit;
import com.mooo.mrausch.poker.objects.Value;


/**
 * This tests the PokerHandLogic
 * @author Michelle Rausch
 *
 */
public class HandLogicTest {

	/**
	 * Runs the test
	 * @param args Not functional
	 */
	public static void main(String[] args) {
		
		List<Card> cards = new ArrayList<Card>();
		
		//Declare Deck, just to have a list of cards, that can be sorted
		Deck deck = new Deck();
		deck.shuffle();
		
		
		PokerHandLogic logic = new PokerHandLogic(deck.getCards());
		
		logic.sortCards();
		
		System.out.println("cards have benn shuffeled the sorted");
		
		logic.getCards().forEach(card -> {
			System.out.println(card.getDescription());
		});
		
		System.out.println("\n-------------NEXT TEST-------------\n" );
		
		System.out.println("invalid number of cards");
		logic.analyzeCards();
		
		System.out.println("\n-------------NEXT TEST-------------\n" );
		logic.getCards().clear();
		System.out.println("scan for Straight Flush");
		cards.add(new Card(Suit.CLUB, Value.FIVE));
		cards.add(new Card(Suit.CLUB, Value.SIX));
		cards.add(new Card(Suit.CLUB, Value.SEVEN));
		cards.add(new Card(Suit.CLUB, Value.EIGHT));
		cards.add(new Card(Suit.CLUB, Value.FOUR));		


		logic.setCards(cards);//sets the cards and sorts them, then runs analysis (TODO)
		
		logic.getCards().forEach(card -> {
			System.out.println(card.getDescription());
		});

		
		logic.sortCards(); //is unnecissary, since cards is already sorted

		System.out.println("This should now say Straight Flush Eight");

		
		System.out.println(logic.getPokerHand());
		System.out.println(logic.getValue());
		System.out.println("\n-------------NEXT TEST-------------\n" );
		System.out.println("There should now not enough cards: ");
		cards.remove(2);
		logic.setCards(cards);
		
		System.out.println("\n-------------NEXT TEST-------------\n" );
		System.out.println("no Straight flush, continue to Four of a Kind scan ");
		
		/*
		 * Four of a kind: 4 cards with the same value. Ranked by the value of the 4 cards.
		 */
		cards.clear();
		cards.add(new Card(Suit.CLUB, Value.FIVE));
		cards.add(new Card(Suit.SPADE, Value.FIVE));
		cards.add(new Card(Suit.HEART, Value.FIVE));
		cards.add(new Card(Suit.CLUB, Value.FIVE));
		cards.add(new Card(Suit.CLUB, Value.FOUR));	


		logic.setCards(cards);
		
		System.out.println("This should now say Four of A Kind Five ");
		System.out.println(logic.getPokerHand());
		System.out.println(logic.getValue());
		
		
		System.out.println("\n-------------NEXT TEST-------------\n" );
		System.out.println("No FoaK, try Full House next");
		/*
		 *  Full House: 3 cards of the same value, with the remaining 2 cards forming a pair. Ranked by the value of the 3 cards.
		 */
		cards.clear();
		cards.add(new Card(Suit.CLUB, Value.FIVE));
		cards.add(new Card(Suit.SPADE, Value.FIVE));
		cards.add(new Card(Suit.HEART, Value.FIVE));
		cards.add(new Card(Suit.SPADE, Value.FOUR));
		cards.add(new Card(Suit.CLUB, Value.FOUR));	
		
		logic.setCards(cards);
		System.out.println("This should now say Full House Five ");
		System.out.println(logic.getPokerHand());
		System.out.println(logic.getValue());
		
		System.out.println("\n-------------NEXT TEST-------------\n" );
		System.out.println("No Full House, try Flush next");
		/*
		 * Flush contains 5 cards of the same suit. Hands which are both flushes are ranked using the rules for High Card.
		 */
		
		cards.clear();
		cards.add(new Card(Suit.CLUB, Value.FIVE));
		cards.add(new Card(Suit.CLUB, Value.SIX));
		cards.add(new Card(Suit.CLUB, Value.ACE));
		cards.add(new Card(Suit.CLUB, Value.KING));
		cards.add(new Card(Suit.CLUB, Value.FOUR));	
		
		logic.setCards(cards);
		System.out.println("This should now say Flush Ace ");
		System.out.println(logic.getPokerHand());
		System.out.println(logic.getValue());
		
		System.out.println("\n-------------NEXT TEST-------------\n" );
		System.out.println("No Full House, try Straight next");
		/*
		 * Straight: Hand contains 5 cards with consecutive values. Hands which both contain a straight are ranked by their highest card.
		 */
		
		cards.clear();
		cards.add(new Card(Suit.CLUB, Value.FIVE));
		cards.add(new Card(Suit.SPADE, Value.SIX));
		cards.add(new Card(Suit.HEART, Value.FOUR));
		cards.add(new Card(Suit.CLUB, Value.EIGHT));
		cards.add(new Card(Suit.CLUB, Value.SEVEN));	
		
		logic.setCards(cards);
		System.out.println("This should now say Straight Eight ");
		System.out.println(logic.getPokerHand());
		System.out.println(logic.getValue());
		
		System.out.println("\n-------------NEXT TEST-------------\n" );
		System.out.println("No Straight, try Three  of a Kind next");
		/*
		 *Three of a Kind: Three of the cards in the hand have the same value. Hands which both contain three of a kind are ranked by the value of the 3 cards.
		 */
		
		cards.clear();
		cards.add(new Card(Suit.CLUB, Value.FIVE));
		cards.add(new Card(Suit.SPADE, Value.ACE));
		cards.add(new Card(Suit.HEART, Value.FIVE));
		cards.add(new Card(Suit.SPADE, Value.FIVE));
		cards.add(new Card(Suit.CLUB, Value.SEVEN));	
		
		logic.setCards(cards);
		System.out.println("This should now say Three of a Kind Five ");
		System.out.println(logic.getPokerHand());
		System.out.println(logic.getValue());
		
		System.out.println("\n-------------NEXT TEST-------------\n" );
		System.out.println("No ToaK, try 2Pair next");
		/*
		 *Two Pairs: The hand contains 2 different pairs. Hands which both contain 2 pairs are ranked by the value of their highest pair. 
		 *Hands with the same highest pair are ranked by the value of their other pair. 
		 *If these values are the same the hands are ranked by the value of the remaining card. TODO: RELEVANT FOR COMPARISON!
		 */

			
		cards.clear();
		cards.add(new Card(Suit.CLUB, Value.FIVE));
		cards.add(new Card(Suit.SPADE, Value.ACE));
		cards.add(new Card(Suit.HEART, Value.FIVE));
		cards.add(new Card(Suit.SPADE, Value.ACE));
		cards.add(new Card(Suit.CLUB, Value.SEVEN));	//For comparison
		
		logic.setCards(cards);
		System.out.println("This should now say 2 Pair ACE ");
		System.out.println(logic.getPokerHand());
		System.out.println(logic.getValue());
			
	
		System.out.println("\n-------------NEXT TEST-------------\n" );
		System.out.println("TwoPair, try Pair next");
		/*
		 * Pair: 2 of the 5 cards in the hand have the same value. 
		 * Hands which both contain a pair are ranked by the value of the cards forming the pair. 
		 * If these values are the same, the hands are ranked by the values of the cards not forming the pair, in decreasing order. TODO: RELEVANT FOR COMPARISON!
		 */
			
		cards.clear();
		cards.add(new Card(Suit.CLUB, Value.FIVE));
		cards.add(new Card(Suit.SPADE, Value.FOUR));
		cards.add(new Card(Suit.HEART, Value.FIVE));
		cards.add(new Card(Suit.SPADE, Value.ACE));
		cards.add(new Card(Suit.CLUB, Value.SEVEN));
		
		logic.setCards(cards);
		System.out.println("This should now say Pair FIVE ");
		System.out.println(logic.getPokerHand());
		System.out.println(logic.getValue());
		
		
		System.out.println("\n-------------NEXT TEST-------------\n" );
		System.out.println("Pair, do High Card");
		/*
		 * High Card: Hands which do not fit any higher category are ranked by the value of their highest card. 
		 * If the highest cards have the same value, the hands are ranked by the next highest, and so on. TODO: RELEVANT FOR COMPARISON!
		 */
			
		cards.clear();
		cards.add(new Card(Suit.CLUB, Value.JACK));
		cards.add(new Card(Suit.SPADE, Value.FOUR));
		cards.add(new Card(Suit.HEART, Value.FIVE));
		cards.add(new Card(Suit.SPADE, Value.ACE));
		cards.add(new Card(Suit.CLUB, Value.SEVEN));
		
		logic.setCards(cards);
		System.out.println("This should now say High Card ACE ");
		System.out.println(logic.getPokerHand());
		System.out.println(logic.getValue());
	}

}
