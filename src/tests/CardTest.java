/**
 * Poker
 * 
 * The aim of this app is to compare 2 hands in a poker-game. 
 * 2 hand have cards assigned to them and this app then outputs who has the better hand according to the rules of poker
 * 
 * This App is a coding sample for a Job-application.
 * 
 * @author Michelle Rausch
 */
package tests;

import com.mooo.mrausch.poker.objects.Card;
import com.mooo.mrausch.poker.objects.Suit;
import com.mooo.mrausch.poker.objects.Value;

/**
 * This tests the functionality of a card
 * @author Michelle Rausch
 *
 */
public class CardTest {

	/**
	 * runs the test
	 * @param args Not functional
	 */
	public static void main(String[] args) {
		
		/*
		 * A card is supposed to have a Suit {C,D,H,S}
		 * 		unordered enum
		 * a number {2,...,9,T,J,Q,K,A} 
		 * 		The 10 is called T as by instruction
		 * 		ordered like shown above
		 * 
		 * implementation:
		 * 	Card IS an Object
		 * 	HAS a suit and 
		 * 	HAS a number (both enums)	
		 * 
		 * a Card can
		 * 	not really do anything
		 *  get a description of card e.g. "ace of spades"
		 */
		Card card;
		
		System.out.println("create sample card");
		
		card = new Card(Suit.HEART, Value.EIGHT);
		

	
		System.out.println(card.toString());
		
		System.out.println("This should yield eight of heart:");
		
		System.out.println(card.getDescription());
		
		System.out.println("This should yield ace of spades:");
		System.out.println(new Card(Suit.SPADE, Value.ACE).getDescription()); // *insert Motörhead music*
		
	}

}
