/**
 * Poker
 * 
 * The aim of this app is to compare 2 hands in a poker-game. 
 * 2 hand have cards assigned to them and this app then outputs who has the better hand according to the rules of poker
 * 
 * This App is a coding sample for a Job-application.
 * 
 * @author Michelle Rausch
 */
package tests;

import com.mooo.mrausch.poker.logic.HandRanker;
import com.mooo.mrausch.poker.logic.PokerHandLogic;
import com.mooo.mrausch.poker.objects.Card;
import com.mooo.mrausch.poker.objects.Deck;
import com.mooo.mrausch.poker.objects.Hand;
import com.mooo.mrausch.poker.objects.Suit;
import com.mooo.mrausch.poker.objects.Value;


/**
 * This tests the comparison of hands
 * If this works, I get the job
 * @author Michelle Rausch
 *
 */
public class HandRankerTest {

	/**
	 * Runs the test
	 * @param args Not functional
	 */
	public static void main(String[] args) {
		/*
		 * The following tests will be performed:
		 * 		2 random hands will be assigned, named (e.g. full house) and compared by rank
		 */
		
		System.out.println("------------------RANDOM HANDS------------------");
		
		Deck deck = new Deck();
		deck.shuffle();
		
		Hand hand = new Hand();
		for (int i = 0; i < 5; i++) {
			hand.takeCardFromDeck(deck);	
		}
		System.out.println("------Hand 1 has pulled:");
		hand.printCards();
		
		Hand otherHand = new Hand();
		for (int i = 0; i < 5; i++) {
			otherHand.takeCardFromDeck(deck);		
		}

		System.out.println("------Hand 2 has pulled:");
		otherHand.printCards();
		
		/*
		 * This has already been tested in HandLogicTest
		 */
		PokerHandLogic logic = new PokerHandLogic(hand);
		System.out.println("Hand 1 has a " +  logic.getPokerHandFull() );
		PokerHandLogic otherLogic = new PokerHandLogic(otherHand);
		System.out.println("Hand 2 has a " +  otherLogic.getPokerHandFull() );
		
		System.out.println("------AND THE WINNER IS:");
		
		
		HandRanker ranker = new HandRanker();
		
		int handRankComparison = ranker.compare(hand, otherHand);
		if (handRankComparison == 0) {
			System.out.println("It's a draw!");
		} else {
			if (handRankComparison > 0) {
				System.out.println("Hand 1 wins!");
			} else {
				System.out.println("Hand 2 wins!");
			}
		}
		
		/*
		 * Now try the Draw resolutions
		 */
		
		System.out.println("------------------DRAW RESOLUTION------------------");
		
		/*
		 * High Card Draw resolution
		 */
		System.out.println("--------High Card:");
		hand.discardAllCards();
		hand.getCards().add(new Card(Suit.CLUB, Value.FOUR));
		hand.getCards().add(new Card(Suit.HEART, Value.SIX));
		hand.getCards().add(new Card(Suit.SPADE, Value.EIGHT));
		hand.getCards().add(new Card(Suit.DIAMOND, Value.JACK));
		hand.getCards().add(new Card(Suit.CLUB, Value.KING));
		
		otherHand.discardAllCards();
		otherHand.getCards().add(new Card(Suit.CLUB, Value.FIVE));
		otherHand.getCards().add(new Card(Suit.HEART, Value.SIX));
		otherHand.getCards().add(new Card(Suit.SPADE, Value.EIGHT));
		otherHand.getCards().add(new Card(Suit.DIAMOND, Value.JACK));
		otherHand.getCards().add(new Card(Suit.CLUB, Value.KING));
		
		
		
		handRankComparison = ranker.compare(hand, otherHand);
		if (handRankComparison < 0) {
			System.out.println("Hand 2 wins!, test sucess");
		}  else {
			System.out.println("Test failed");
		}
		
		
		hand.discardAllCards();
		hand.getCards().add(new Card(Suit.CLUB, Value.FOUR));
		hand.getCards().add(new Card(Suit.HEART, Value.SIX));
		hand.getCards().add(new Card(Suit.SPADE, Value.EIGHT));
		hand.getCards().add(new Card(Suit.DIAMOND, Value.JACK));
		hand.getCards().add(new Card(Suit.CLUB, Value.KING));
		
		otherHand.discardAllCards();
		otherHand.getCards().add(new Card(Suit.CLUB, Value.FOUR));
		otherHand.getCards().add(new Card(Suit.HEART, Value.SIX));
		otherHand.getCards().add(new Card(Suit.SPADE, Value.EIGHT));
		otherHand.getCards().add(new Card(Suit.DIAMOND, Value.JACK));
		otherHand.getCards().add(new Card(Suit.CLUB, Value.KING));
		
		
		
		handRankComparison = ranker.compare(hand, otherHand);
		if (handRankComparison == 0) {
			System.out.println("It's a draw!, test success");
		}  else {
			System.out.println("Test failed");
		}
		
		/*
		 * Pair Draw resolution
		 */
		System.out.println("--------Pair:");
		
		hand.discardAllCards();
		hand.getCards().add(new Card(Suit.CLUB, Value.FOUR));
		hand.getCards().add(new Card(Suit.HEART, Value.FOUR));
		hand.getCards().add(new Card(Suit.SPADE, Value.EIGHT));
		hand.getCards().add(new Card(Suit.DIAMOND, Value.JACK));
		hand.getCards().add(new Card(Suit.CLUB, Value.KING));
		
		otherHand.discardAllCards();
		otherHand.getCards().add(new Card(Suit.CLUB, Value.FOUR));
		otherHand.getCards().add(new Card(Suit.HEART, Value.FOUR));
		otherHand.getCards().add(new Card(Suit.SPADE, Value.EIGHT));
		otherHand.getCards().add(new Card(Suit.DIAMOND, Value.JACK));
		otherHand.getCards().add(new Card(Suit.CLUB, Value.KING));
		
		handRankComparison = ranker.compare(hand, otherHand);
		if (handRankComparison == 0) {
			System.out.println("It's a draw!, test success");
		}  else {
			System.out.println("Test failed");
		}
		
		
		hand.discardAllCards();
		hand.getCards().add(new Card(Suit.CLUB, Value.FOUR));
		hand.getCards().add(new Card(Suit.HEART, Value.FOUR));
		hand.getCards().add(new Card(Suit.SPADE, Value.NINE));
		hand.getCards().add(new Card(Suit.DIAMOND, Value.JACK));
		hand.getCards().add(new Card(Suit.CLUB, Value.KING));
		
		otherHand.discardAllCards();
		otherHand.getCards().add(new Card(Suit.CLUB, Value.FOUR));
		otherHand.getCards().add(new Card(Suit.HEART, Value.FOUR));
		otherHand.getCards().add(new Card(Suit.SPADE, Value.EIGHT));
		otherHand.getCards().add(new Card(Suit.DIAMOND, Value.JACK));
		otherHand.getCards().add(new Card(Suit.CLUB, Value.KING));
		
		handRankComparison = ranker.compare(hand, otherHand);
		if (handRankComparison == 0) {
			System.out.println("It's Hand1, test success");
		} else {
			System.out.println("Test failed");
		}
		
		/*
		 * Two Pair draw resolution
		 */
		System.out.println("--------Two Pair:");
		hand.discardAllCards();
		hand.getCards().add(new Card(Suit.CLUB, Value.FOUR));
		hand.getCards().add(new Card(Suit.HEART, Value.FOUR));
		hand.getCards().add(new Card(Suit.SPADE, Value.EIGHT));
		hand.getCards().add(new Card(Suit.DIAMOND, Value.EIGHT));
		hand.getCards().add(new Card(Suit.CLUB, Value.KING));
		
		otherHand.discardAllCards();
		otherHand.getCards().add(new Card(Suit.CLUB, Value.FOUR));
		otherHand.getCards().add(new Card(Suit.HEART, Value.FOUR));
		otherHand.getCards().add(new Card(Suit.SPADE, Value.EIGHT));
		otherHand.getCards().add(new Card(Suit.DIAMOND, Value.EIGHT));
		otherHand.getCards().add(new Card(Suit.CLUB, Value.KING));
		
		handRankComparison = ranker.compare(hand, otherHand);
		if (handRankComparison == 0) {
			System.out.println("It's a draw!, test success");
		}  else {
			System.out.println("Test failed");
		}
		
		
		hand.discardAllCards();
		hand.getCards().add(new Card(Suit.CLUB, Value.FOUR));
		hand.getCards().add(new Card(Suit.HEART, Value.FOUR));
		hand.getCards().add(new Card(Suit.SPADE, Value.EIGHT));
		hand.getCards().add(new Card(Suit.DIAMOND, Value.EIGHT));
		hand.getCards().add(new Card(Suit.CLUB, Value.KING));
		
		otherHand.discardAllCards();
		otherHand.getCards().add(new Card(Suit.CLUB, Value.FOUR));
		otherHand.getCards().add(new Card(Suit.HEART, Value.FOUR));
		otherHand.getCards().add(new Card(Suit.SPADE, Value.EIGHT));
		otherHand.getCards().add(new Card(Suit.DIAMOND, Value.EIGHT));
		otherHand.getCards().add(new Card(Suit.CLUB, Value.QUEEN));
		
		handRankComparison = ranker.compare(hand, otherHand);
		if (handRankComparison > 0) {
			System.out.println("It's Hand1, test success");
		} else {
			System.out.println("Test failed");
		}
		
		
		/*
		 * ToaK draw resolution
		 */
		System.out.println("--------ThreeOfAKind:");
		hand.discardAllCards();
		hand.getCards().add(new Card(Suit.CLUB, Value.FOUR));
		hand.getCards().add(new Card(Suit.HEART, Value.FOUR));
		hand.getCards().add(new Card(Suit.SPADE, Value.FOUR));
		hand.getCards().add(new Card(Suit.DIAMOND, Value.EIGHT));
		hand.getCards().add(new Card(Suit.CLUB, Value.KING));
		
		otherHand.discardAllCards();
		otherHand.getCards().add(new Card(Suit.CLUB, Value.FOUR));
		otherHand.getCards().add(new Card(Suit.HEART, Value.FOUR));
		otherHand.getCards().add(new Card(Suit.SPADE, Value.FOUR));
		otherHand.getCards().add(new Card(Suit.DIAMOND, Value.EIGHT));
		otherHand.getCards().add(new Card(Suit.CLUB, Value.KING));
		
		handRankComparison = ranker.compare(hand, otherHand);
		if (handRankComparison == 0) {
			System.out.println("It's a draw!, test success");
		}  else {
			System.out.println("Test failed");
		}
		
		
		hand.discardAllCards();
		hand.getCards().add(new Card(Suit.CLUB, Value.FOUR));
		hand.getCards().add(new Card(Suit.HEART, Value.FOUR));
		hand.getCards().add(new Card(Suit.SPADE, Value.FOUR));
		hand.getCards().add(new Card(Suit.DIAMOND, Value.EIGHT));
		hand.getCards().add(new Card(Suit.CLUB, Value.KING));
		
		otherHand.discardAllCards();
		otherHand.getCards().add(new Card(Suit.CLUB, Value.FOUR));
		otherHand.getCards().add(new Card(Suit.HEART, Value.FOUR));
		otherHand.getCards().add(new Card(Suit.SPADE, Value.EIGHT));
		otherHand.getCards().add(new Card(Suit.DIAMOND, Value.EIGHT));
		otherHand.getCards().add(new Card(Suit.CLUB, Value.QUEEN));
		
		handRankComparison = ranker.compare(hand, otherHand);
		if (handRankComparison > 0) {
			System.out.println("It's Hand1, test success");
		} else {
			System.out.println("Test failed");
		}
		
		/*
		 * Full House Draw resolution
		 */
		System.out.println("--------Full House:");
		hand.discardAllCards();
		hand.getCards().add(new Card(Suit.CLUB, Value.FOUR));
		hand.getCards().add(new Card(Suit.HEART, Value.FOUR));
		hand.getCards().add(new Card(Suit.SPADE, Value.FOUR));
		hand.getCards().add(new Card(Suit.DIAMOND, Value.EIGHT));
		hand.getCards().add(new Card(Suit.CLUB, Value.EIGHT));
		
		otherHand.discardAllCards();
		otherHand.getCards().add(new Card(Suit.CLUB, Value.FOUR));
		otherHand.getCards().add(new Card(Suit.HEART, Value.FOUR));
		otherHand.getCards().add(new Card(Suit.SPADE, Value.FOUR));
		otherHand.getCards().add(new Card(Suit.DIAMOND, Value.EIGHT));
		otherHand.getCards().add(new Card(Suit.CLUB, Value.EIGHT));
		
		handRankComparison = ranker.compare(hand, otherHand);
		if (handRankComparison == 0) {
			System.out.println("It's a draw!, test success");
		}  else {
			System.out.println("Test failed");
		}
		
		
		hand.discardAllCards();
		hand.getCards().add(new Card(Suit.CLUB, Value.FOUR));
		hand.getCards().add(new Card(Suit.HEART, Value.FOUR));
		hand.getCards().add(new Card(Suit.SPADE, Value.FOUR));
		hand.getCards().add(new Card(Suit.DIAMOND, Value.TEN));
		hand.getCards().add(new Card(Suit.CLUB, Value.TEN));
		
		otherHand.discardAllCards();
		otherHand.getCards().add(new Card(Suit.CLUB, Value.FOUR));
		otherHand.getCards().add(new Card(Suit.HEART, Value.FOUR));
		otherHand.getCards().add(new Card(Suit.SPADE, Value.FOUR));
		otherHand.getCards().add(new Card(Suit.DIAMOND, Value.NINE));
		otherHand.getCards().add(new Card(Suit.CLUB, Value.NINE));
		
		handRankComparison = ranker.compare(hand, otherHand);
		if (handRankComparison > 0) {
			System.out.println("It's Hand1, test success");
		} else {
			System.out.println("Test failed");
		}
	}

}
