/**
 * Poker
 * 
 * The aim of this app is to compare 2 hands in a poker-game. 
 * 2 hand have cards assigned to them and this app then outputs who has the better hand according to the rules of poker
 * 
 * This App is a coding sample for a Job-application.
 * 
 * @author Michelle Rausch
 */
package tests;

import com.mooo.mrausch.poker.objects.Deck;
import com.mooo.mrausch.poker.objects.Hand;

/**
 * This class tests the functionality of the poker-hand
 * @author Michelle Rausch
 *
 */
public class HandTest {

	/**
	 * Runs the test
	 * @param args Not functional
	 */
	public static void main(String[] args) {
		/*
		 * A Hand contains 5 Card
		 * 
		 * A Hand can have a rank from HighCard to StraightFlush (Value low to high)
		 * 
		 * hand can
		 * 		discard card -> to deck
		 * 		pull card from deck
		 * 		print all cards
		 * 	
		 */
		
		
		Deck deck = new Deck();
		
		Hand hand = new Hand();
		
		Hand otherHand = new Hand();
		
		deck.shuffle();
		
		/*
		 * Pull 5 cards from deck to each hand
		 */
		
		for (int i = 0; i < 5; i++) {
			hand.takeCardFromDeck(deck);
		}
		
		
		for (int i = 0; i < 5; i++) {
			otherHand.takeCardFromDeck(deck);
		}
		
		
		/*
		 * Name The hand
		 */
		
		System.out.println("Hand 1 has:");
		hand.printCards();
		
//		System.out.println("This is a " +  hand.getPokerHand());
		
		
		
		
		

	}

}
